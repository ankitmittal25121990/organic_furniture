# Organic Furniture

Inflatable air furniture are manufactured using materials such as nylon, natural rubber, polyvinyl chloride (PVC) and polyurethane. They are light and cheap. The only downside is that air furniture are not as comfortable foam-based furniture. Be mindful of tech based ads and commercials that are actually glib.

## Blended foam furniture

Blended foam is the result of a mix of different plant based materials, memory foam and some latex or plant oil in certain percentages.  Also known as synthetic foam, it has a seemingly natural feel. This way, companies can brag about a perfect balance between safe materials and affordability.  At Natural Mattress Matters  we offer high quality [organic furniture](https://www.naturalmattressmatters.com) with all those toxic chemials that conventional companies insert in the furniture.  
